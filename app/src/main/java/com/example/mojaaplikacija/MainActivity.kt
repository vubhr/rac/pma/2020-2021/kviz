package com.example.mojaaplikacija

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

private const val TAG = "MainActivity"
class MainActivity : AppCompatActivity() {

    private lateinit var btnTrue : Button
    private lateinit var btnFalse : Button
    private lateinit var btnNext : Button
    private lateinit var tvQuestion: TextView

    private val questionList = listOf(
        Question(R.string.question_subject, false),
        Question(R.string.question_android, false),
        Question(R.string.question_cro, true))

    private var currentIndex = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d(TAG, "OnCreate")
        btnTrue = findViewById(R.id.btnTrue)
        btnFalse = findViewById(R.id.btnFalse)
        btnNext = findViewById(R.id.btnNext)
        tvQuestion = findViewById(R.id.tvQuestion)

        btnTrue.setOnClickListener{v: View? ->
            checkAnswer(true)
        }
        btnFalse.setOnClickListener{v: View? ->
            checkAnswer(false)
        }
        btnNext.setOnClickListener{v: View? ->
            currentIndex = (currentIndex + 1) % questionList.size
            updateQuestion()
        }
        updateQuestion()
    }

    private fun updateQuestion(){
        var questionTextResId = questionList[currentIndex].textResId;
        tvQuestion.setText(questionTextResId)

    }
    private fun checkAnswer(userAnswer: Boolean){
        val correctAnswer = questionList[currentIndex].answer
        val messageResId = if (userAnswer == correctAnswer) {
            R.string.toast_true
        } else {
            R.string.toast_false
        }
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT)
            .show()
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "OnStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "OnResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "OnPause")

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "OnDestroy")

    }


}